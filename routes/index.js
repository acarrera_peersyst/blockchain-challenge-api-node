var express = require('express');
var router = express.Router();
var user = require('../models/user.js');
const web3 = require('web3');
var residusABI = require('../models/residusABI');

const contractAddress='0x40061440badf7db16e0b9fd66e93b16ab17cd498';
const web3js = new web3(new web3.providers.HttpProvider("https://rinkeby.infura.io/v3/718c05adc4d44d4e8ba562f161bb6a69"));
const web3jswss = new web3(new web3.providers.WebsocketProvider("wss://rinkeby.infura.io/ws"));
const residusContractEvents = new web3jswss.eth.Contract(residusABI, contractAddress);
const residusContract = new web3js.eth.Contract(residusABI, contractAddress);


residusContractEvents.events.OpenContainer({
    filter: {containerId: 1},
    fromBlock: 0
}, function(error, event){ console.log('asdf'); console.log(error); console.log(event); })
    .on('data', function(event){
        console.log('data');
        console.log(event); // same results as the optional callback above
    })
    .on('changed', function(event){
        console.log('changed');
        console.log(event);
        // remove event from local database
    })
    .on('error', console.error);



router.get('/get-user', function(req, res, next) {
    const id = req.query.id;
    user.getById(id, function(err, user) {
        if (err) res.send(err);
        res.json(user[0]);
    });
});

router.get('/deposit', function (req, res, next) {
    const userId = req.query.userId;
    const containerId = req.query.containerId;

    user.getById(userId, (err, user) => {
        //TODO: Undo hardcoded tokens received
        const transaction = residusContract.methods.deposit(userId, containerId, 10);
        web3js.eth.getTransactionCount(user[0].address).then( nonce => {
            let options = {
                to  : transaction._parent._address,
                data: transaction.encodeABI(),
                gasPrice: '20000000000',
                gas: 1000000,
                nonce: nonce
            };
            web3js.eth.accounts.signTransaction(options, user[0].private_key).then(signedTransaction => {
                web3js.eth.sendSignedTransaction(signedTransaction.rawTransaction).then( result => {
                    res.json(result);
                });
            });
        });
    });


});

module.exports = router;
