pragma solidity ^0.4.19;
pragma experimental ABIEncoderV2;

import "./ownable.sol";

contract Residus is Ownable {

    event OpenContainer(uint containerId);

    struct Container {
        uint id;
        string containerType;
    }

    struct Deposit {
        uint containerId;
        string containerType;
        int operationEcos;
        uint timestamp;
    }

    mapping (address => uint) users;

    mapping (uint => Container) containers;
    mapping (uint => Deposit[]) deposits;
    mapping (uint => int) ecos;

    function deposit(uint _userId, uint _containerId, int _operationEcos) public onlyOwner {
        //require(users[msg.sender] == _userId);

        OpenContainer(_containerId);
        Container memory container = containers[_containerId];
        deposits[_userId].push(Deposit(container.id, container.containerType, _operationEcos, now));
        ecos[_userId] += _operationEcos;
    }

    function addContainer(uint _containerId, string _containerType) public onlyOwner {
        containers[_containerId] = Container(_containerId, _containerType);
    }

    function assignAddressToUser(uint _userId, address _address) public onlyOwner {
        users[_address] = _userId;
    }

    function getUserEcos(uint _userId) public view returns (int) {
        return ecos[_userId];
    }

    function getUserDeposits(uint _userId) public view returns (Deposit[]) {
        return deposits[_userId];
    }
}
