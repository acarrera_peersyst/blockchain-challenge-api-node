'user strict';
const sql = require('./db.js');

//Task object constructor
const User = function(user){
    this.id = user.id;
    this.name = user.name;
    this.dni = user.dni;
    this.address = user.address;
    this.private_key = user.private_key;
};

User.getById = function (userId, result) {
    console.log(userId);
    sql.query("Select * from users where id = ? ", userId, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);

        }
    });
};

module.exports= User;
